AC_DEFUN([CHECK_OMP],
[
    omp_error="no"
    check_omp="yes"
    force_omp="no"
    have_omp="no"

    AC_ARG_VAR(OPENMP_CXXFLAGS, [C++ compiler flags to enable support for OpenMP])

    AC_ARG_WITH([omp],
        AC_HELP_STRING([--with-omp],
            [use OpenMP for some OTF tools, default: yes if found by configure]),
        [if test "$withval" = "yes"; then force_omp="yes"; else check_omp="no"; fi])

    AS_IF([test "$check_omp" = "yes"],
    [
        AC_LANG_SAVE
        AC_LANG_CPLUSPLUS
        AX_OPENMP([have_omp="yes"], [omp_error="yes"])
        AC_LANG_RESTORE
    ])
])
