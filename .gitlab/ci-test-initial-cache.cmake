#
# Default options for the gitlab CI test configurations
#
set( BUILD_SHARED_LIBS ON CACHE BOOL "" )

set( CMAKE_INSTALL_PREFIX "$ENV{PWD}/../install" CACHE PATH "" )
set( CMAKE_VERBOSE_MAKEFILE ON CACHE BOOL "" )
set( CMAKE_EXPORT_COMPILE_COMMANDS ON CACHE BOOL "" )

if ( (DEFINED VITE_CI_BRANCH) AND ("${VITE_CI_BRANCH}" STREQUAL "master") )
  set( CMAKE_BUILD_TYPE RelWithDebInfo CACHE STRING "" FORCE )
  set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Werror" CACHE STRING "" )
else()
  set( CMAKE_BUILD_TYPE Debug CACHE STRING "" FORCE )
  set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O0 -g -Werror" CACHE STRING "" )
endif()

option(VITE_ENABLE_WARNING  "Enable warning messages"        ON)
option(VITE_ENABLE_COVERAGE "Enable flags for coverage test" ON)

