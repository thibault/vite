#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include "OTF-1.2.18/otflib/otf.h"

#define NBPHIL 5
#define NB_STATES 3

/* gcc -g -W -Wall philosophersOTF.c -o test_otf_writer -lpthread -lrt -IOTF-1.2.18/otflib -LOTF-1.2.18/otflib -lotf -lz && ./test_otf_writer */

enum state {
    THINK  = 0,
    HUNGRY = 1,
    EAT    = 2
};

#define SEM_ID 1

pthread_mutex_t  mutex = PTHREAD_MUTEX_INITIALIZER;
sem_t           *semphil;
int              philosophes[5];
uint64_t           timestamp = 0;
OTF_FileManager *manager;
OTF_Writer      *writer;
uint32_t        *children;

void  *thread_function(void *arg);
void   eat            (int   time);
void   think          (int   time);
void   take_forks     (int   i);
void   release_forks  (int   i);
void   test           (int   i);
uint64_t clockGet       (void);

void *thread_function(void * arg)
{
    int i;
    int *tab = (int *)arg;
  
    for (i = 0; i < NBPHIL ; i++)
        {
            think(tab[1]);
            take_forks(tab[0]);
            eat(tab[2]);
            release_forks(tab[0]);
        }
    return (void*)NULL;
}

void eat(int time)
{
    sleep(time);
}

void think(int time)
{
    sleep(time);
}

void take_forks(int i)
{
    int ret = 0;
    double time = clockGet();
    pthread_mutex_lock(&mutex);
    ret = OTF_Writer_writeLeave (writer, time, philosophes[i], children[i], 0);

    if(ret == 0)
        {
            perror("write leave");
        }
    philosophes[i] = HUNGRY;

    time = clockGet();
    ret = OTF_Writer_writeEnter (writer, time, philosophes[i], children[i], 0);
    if(ret == 0)
        {
            perror("write enter");
        }
    test(i);
    pthread_mutex_unlock(&mutex);
    sem_wait(&(semphil[i])); 

    OTF_Writer_writeCounter (writer, clockGet(), i+2, SEM_ID, 0);
}

void release_forks(int i)
{
    pthread_mutex_lock(&mutex);
    OTF_Writer_writeLeave (writer, clockGet(), philosophes[i], children[i], 0);
    philosophes[i] = THINK;
    OTF_Writer_writeEnter (writer, clockGet(), philosophes[i], children[i], 0);
    test((i+NBPHIL-1)%NBPHIL);
    test((i+NBPHIL+1)%NBPHIL);
    pthread_mutex_unlock(&mutex);
}

void test(int i)
{
    if ((philosophes[i] == HUNGRY) &&
        (philosophes[(i+NBPHIL-1)%NBPHIL] != EAT) && 
        (philosophes[(i+NBPHIL+1)%NBPHIL] != EAT))
        {
            OTF_Writer_writeLeave (writer, clockGet(), philosophes[i], children[i], 0);
            philosophes[i] = EAT;
            OTF_Writer_writeCounter (writer, clockGet(), i+2, SEM_ID, 1);
            OTF_Writer_writeEnter (writer, clockGet(), philosophes[i], children[i], 0);
            sem_post(&semphil[i]);
        }
}

uint64_t clockGet (void)
{
    struct timespec tp;
  
    clock_gettime (CLOCK_REALTIME, &tp);            /* Elapsed time */

    return (( tp.tv_sec * 1.0e9L + tp.tv_nsec) - timestamp);
}

int main(int argc, char *argv[])
{
    pthread_t *calltab;
    int       *param;
    int        i;
    char       name[10]; /* The philosophers' name */
    uint32_t   number_of_streams = 100;
    manager = OTF_FileManager_open(number_of_streams);
    writer = OTF_Writer_open("test_philosophers/philosophers.otf", number_of_streams, manager);
    OTF_HandlerArray* handlers = OTF_HandlerArray_open();
    
    /* Why not ^^ */
    OTF_Writer_setCompression(writer, OTF_FILECOMPRESSION_UNCOMPRESSED);/* OTF_FILECOMPRESSION_COMPRESSED */
    
    OTF_Writer_writeDefTimerResolution(writer, 1, 1.0e9L);

    if(manager == NULL)
        {
            printf("manager failed\n");
            return EXIT_FAILURE;
        }
    if(writer == NULL)
        {
            printf("open failed!!\n");
            return EXIT_FAILURE;
        }
    
    if(handlers == NULL)
        {
            printf("handlers failed!!\n");
            return EXIT_FAILURE;
        }
    
    /* Type of container and associates states */
    children = (uint32_t *)malloc((NBPHIL+1)*sizeof(uint32_t));
    
    /* Create the parent container */
    OTF_Writer_writeDefProcess(writer, 1, 1, "Programme", 0);
    
    for(i = 0 ; i < NBPHIL ; i ++)
        {
            children[i] = i+2;
            
            /* Create the thread container */
            sprintf(name, "Thread%d", i);
            OTF_Writer_writeDefProcess(writer, 1, children[i], name, 1);
        }
    
    /* States type */
    OTF_Writer_writeDefFunctionGroup(writer, 1, 1, "Thread State");

    /* States */
    OTF_Writer_writeDefFunction(writer, 1, EAT, "Eat", 1, 0);
    OTF_Writer_writeDefFunction(writer, 1, THINK, "Think", 1, 0);
    OTF_Writer_writeDefFunction(writer, 1, HUNGRY, "Hungry", 1, 0);
    
    /* Create semaphore variables -> Counter */
    OTF_Writer_writeDefCounterGroup(writer, 1, 1, "Semaphore");
    OTF_Writer_writeDefCounter(writer, 1, SEM_ID, "counter-name", 0, 1, 0);
    
    /* Allocation, initialisation and let's go */
    semphil = (sem_t *)malloc(NBPHIL*sizeof(sem_t));
    calltab = (pthread_t *)malloc(NBPHIL*sizeof(pthread_t));
    param   = (int *)malloc(3*NBPHIL*sizeof(int));
    timestamp = clockGet();
    
    OTF_Writer_writeBeginProcess (writer, clockGet(), 1);
    
    for (i=0; i<NBPHIL; i++)
        {
            param[3*i]   = i;
            param[3*i+1] = random()%5  + 1;
            param[3*i+2] = random()%10 + 1;
            // +i is for get them in the order ^^
            OTF_Writer_writeBeginProcess (writer, clockGet()+1, i+2);
            OTF_Writer_writeCounter (writer, clockGet()+1, i+2, SEM_ID, 0);
            sem_init(&(semphil[i]), 0, 0);
            pthread_create(&calltab[i], NULL, thread_function, (void *)(param+i*3));
        }
    
    for (i=0; i<NBPHIL; i++)
        {
            pthread_join(calltab[i],(void**)NULL);
            sem_destroy(&(semphil[i]));
            
            /* Destroy each thread container */
            OTF_Writer_writeEndProcess (writer, clockGet(), i+2);
        }
    
    /* End the process */
    OTF_Writer_writeEndProcess (writer, clockGet(), 1);
    
    free(semphil);
    free(calltab);
    free(param);
    free(children);
    OTF_HandlerArray_close(handlers);
    OTF_Writer_close(writer);
/*     OTF_FileManager_close(manager); */
    
    return EXIT_SUCCESS;
}
