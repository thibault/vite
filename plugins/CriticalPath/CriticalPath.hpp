#ifndef CRITICAL_PATH_HPP
#define CRITICAL_PATH_HPP

#include <QFileDialog>
#include "plugin/Plugin.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/Container.hpp"
#include "trace/EntityValue.hpp"
#include "trace/StateChange.hpp"
#include "trace/tree/BinaryTree.hpp"
#include "trace/tree/Node.hpp"
#include "trace/Trace.hpp"

#include "render/vbo/Render_alternate.hpp"
#include "render/opengl/Render_opengl.hpp"

// Generated //
#include "ui_CriticalPath.h"

/*!
 * \class CriticalPath
 * \brief Plugin class that represents statistics: CriticalPath/Max Breadth
 */
class CriticalPath : public Plugin, public Ui::CriticalPath{
    Q_OBJECT

    bool _critical_path_checked = false;
    std::vector<State*> _critical_path_states;
public:
    /*!
     * \fn get_instance()
     * \brief Create a static CriticalPath plugin.
     */
    static CriticalPath* get_instance();

    /*!
     * \fn init()
     * \brief Initialize the CriticalPath plugin
     */
    void init();

    /*!
     * \fn clear()
     * \brief Clear the CriticalPath plugin
     */
    void clear();

    /*!
     * \fn set_arguments(std::map<std::string, QVariant *>)
     * \brief Set the arguments of the CriticalPath plugin. (Unused for now)
     */
    void set_arguments(std::map<std::string /*argname*/, QVariant* /*argValue*/>);

    /*!
     * \fn get_name()
     * \brief Return the name of this plugin
     */
    std::string get_name();

private:
    CriticalPath();
    ~CriticalPath();
    static CriticalPath*   s_plugin;
    /*
     * \brief As the name suggests this functions sets the default line edits (file.dot and file.rec). Sessions are used to remember last path used.
     * \param le QLineEdit name in UI
     * \param name Used in Qt interface to indicates type of file needed for the statistics computing module
     * \param def_name Used in Qt interface to describe type of file needed for the statistics computing module
     */
    void set_line_edit_defaults( QLineEdit *le, std::string name, std::string def_name );

public slots:
    /*
     * \brief What to do when execute button is clicked
     */
    void execute();

private slots:
    /*
     * \brief Button for openning Dag file, path is then put in correspondant QLineEdit in UI
     */
    static void on_tool_button_dag_clicked();

    /*
     * \brief Button for openning Rec file, path is then put in correspondant QLineEdit in UI
     */
    static void on_tool_button_rec_clicked();

    void on_critical_path_stateChanged(int);

};

extern "C" 
#ifdef WIN32
    __declspec(dllexport) // no comment
#endif
/*
 * \brief Initialize the plugin in ViTe
 */
Plugin *create();


#endif // CRITICAL_PATH_HPP
