#ifndef HELPER_HPP
#define HELPER_HPP

enum LogStatus
{
    MESSAGE,
    WARNING,
    FATAL,

    COUNT
};

static inline double
print_get_value( double flops )
{
    static double ratio = (double)(1<<10);
    int unit = 0;

    while ( (flops > ratio) && (unit < 9) ) {
        flops /= ratio;
        unit++;
    }
    return flops;
}

static inline char
print_get_units( double flops )
{
    static char units[9] = { ' ', 'K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y' };
    static double ratio = (double)(1<<10);
    int unit = 0;

    while ( (flops > ratio) && (unit < 9) ) {
        flops /= ratio;
        unit++;
    }
    return units[unit];
}

namespace Helper
{
    void log(LogStatus status, const char* format, ...);
    void set_infos(const char* format, ...);
}

#endif
