#ifndef ZOOM_HPP
#define ZOOM_HPP

#include <QtOpenGL/QtOpenGL>

#include "../Formats/SymbolMatrix.hpp"

/**
* \class Zoom
* \brief This is an abstract class, it contains the attributes and functions that all the sub-classes need
*
*/
class Zoom {
public:
    /**
    * \brief The size of the color matrix
    * 
    */
    static const int DEFAULT_LEVEL = 1024;

public:
    /**
    * \fn Zoom(symbol_matrix_t* matrix)
    * \brief Construct a zoom object
    * \param matrix The matrix where the zoom is applied
    */
    Zoom(symbol_matrix_t* matrix);

    /**
     * \fn virtual ~Zoom()
     * \brief Destroy the Zoom object
     */
    virtual ~Zoom();

    /**
     * \fn GLfloat getColor(int x, int y) const
     * \brief Get the Color in x an y coordinates
     * \param x first coordinate
     * \param y second coordinate
     * \return A color of type GLfloat 
     */
    GLfloat getColor(int x, int y) const;

    /**
     * \fn virtual void move(double x_start, double x_end, double y_start, double y_end) = 0
     * \brief A virtual function, it is the function that is called when the zoom is applied
     * \param x_start The start of x coordinate of the selected zone
     * \param x_end The end of x coordinate of the selected zone
     * \param y_start The start of y coordinate of the selected zone
     * \param y_end The end of y coordinate of the selected zone
     */
    virtual void move(double x_start, double x_end, double y_start, double y_end) = 0;

protected:
    /**
     * \brief A pointer to the matrix to display
     * 
     */
    symbol_matrix_t *m_matrix;

    /**
     * \brief The color matrix that is painted on the display
     * 
     */
    GLfloat          m_colors[DEFAULT_LEVEL][DEFAULT_LEVEL];
};

#endif