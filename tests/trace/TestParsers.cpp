/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément 
**
*/

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <map>

#include "trace/values/Values.hpp"
#include "trace/EntityValue.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/Trace.hpp"
#include "parser/Parser.hpp"
#include "parser/ParserPaje.hpp"
#include "parser/ParserVite.hpp"


using namespace std;

int main(int argc, char **argv) {

    Parser          *parser = NULL;
    Trace           *trace = NULL;
    vector <string>  filenames;
    string           filename;
    unsigned int     idot; 

    if(argc < 2) {
        cerr << "Usage : " << argv[0]
             << " trace1 trace2 ..." << endl;
        return EXIT_FAILURE;
    }
    else {
        for (int i = 0 ; i < argc ; i ++) {
            filenames.push_back(argv[i]);
        }
    }

    for(int i = 1 ; i < argc ; i ++) {
	trace    = new Trace();
        filename = filenames[i];
        idot     = filename.find_last_of('.');

	std::cout << "Fichier : " << filename << std::endl; 

        if (idot != string::npos) {
            if(filename.substr(idot) == ".trace") {
		std::cout << "Parser  : Paje" << std::endl; 
                parser = new ParserPaje(filename);
            }
#ifdef WITH_OTF
            else if(filename.substr(idot) == ".otf") {
		std::cout << "Parser  : OTF" << std::endl; 
                parser = new ParserOTF(filename);
            }
#endif //WITH_OTF
#ifdef WITH_TAU
            else if(filename.substr(idot) == ".tau") {
		std::cout << "Parser  : TAU" << std::endl; 
                parser = new ParserTAU(filename);
            }
#endif //WITH_OTF
            else if(filename.substr(idot) == ".ept") {
		std::cout << "Parser  : ViTE" << std::endl; 
                parser = new ParserVite(filename);
            }
        }
        if(!parser) { // Default
            std::cout << "Parser  : Problem (Paje)" << std::endl; 
            parser = new ParserPaje(filename);
        }
        
        try {
            parser->parse(*trace);
        }
        catch (const std::string &error) {
            cout << error << "  " << filename << endl;
            parser->finish();
        }

        delete parser;
        parser = NULL;
        delete trace;
        trace = NULL;
    }
    return EXIT_SUCCESS;
}
