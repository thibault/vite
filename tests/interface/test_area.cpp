/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developpers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément 
**
*/
/*!
 * \brief Test of the container drawing
 */



#include "test_area.hpp"


using namespace std;

class Interface_ : public Interface{
public:
    void error(string s) const{
        cerr << s << endl;
    }

    void warning(string s) const{
        cerr << s << endl;
    }
    
    void information(string s) const{
        cerr << s << endl;
    }
    
    const std::string get_filename() const{
        return string("Filename");
    }
    
};


int main(int argc, char** argv){
    glutInit(&argc, argv);

    QApplication app(argc, argv);

    Interface_ i;
    Message::set_interface(&i);
   
    //  Interface_graphic* g = new Interface_graphic();
    QWidget t;
    t.setGeometry(0, 0, 800, 600);
   
    //Render_area r(&t);
    //r.setGeometry(0, 0, 800, 600);
    //    r.show();
    t.show();
    

    //r.start_draw();
    {
        
        //r.start_draw_containers();
        {
            /*  r.draw_container(0, 0, 30, 20);
                r.draw_container(0, 22, 10, 20);
                r.draw_container(0, 43, 15, 20);*/
            //  r.draw_container(1, 60, 60, 20);
            //r.draw_container_text(0, 10,"Container 2");
            //r.draw_container_text(0, 32,"Container 1");
        }
        // r.end_draw_containers();
        
        // r.start_draw_states();
        {
       
            /*            r.draw_state(0, 4, 0, 20, 0.6, 0.7, 0.8);
                          r.draw_state(4.5, 7.98, 0, 20, 0.6, 0.7, 0.8);
                          r.draw_arrow(10, 10, 0, 3);
                          r.draw_arrow(10, -10, -15, -3);
                          r.draw_arrow(-150, 150, -10, 30);
                          r.draw_event(14, 4, 2);
                          r.draw_event(10, 4, 1);
                          r.draw_event(18, 4, 3);
                          r.draw_event(10, 4, 10);*/

        }
        //r.end_draw_states();

        //r.start_draw_counter();
        {
            //r.draw_counter(0.0f, 1);
            //r.draw_counter(5, 1.2);
            //r.draw_counter(7, 2);
            //r.draw_counter(19.345, 3);
            //r.draw_counter(24.45, 2);
            //r.draw_counter(32, 6);
        }
        //r.end_draw_counter();
        
     
    }
    //r.end_draw();

    //r. build();

    return app.exec();
}

