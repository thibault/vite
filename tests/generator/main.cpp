#include <QtGui/QApplication>
#include "GeneratorWindow.hpp"


int main (int argc, char** argv){
    QApplication* app = new QApplication(argc,argv);
    GeneratorWindow* g = new GeneratorWindow (NULL, app);
    g->run ();

    return EXIT_SUCCESS;
}
