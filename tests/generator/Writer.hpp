#ifndef _VITE_WRITER_
#define _VITE_WRITER_

#include <stdio.h>
#include <QString>


class Writer {

protected :



    FILE*   _dest;         ; // File destination

public :

    //! Open the file and write the first part of the trace 
    virtual void initTrace (QString name, int depth, int procNbr, int stateNbr, int eventNbr, int linkTypeNbr, int varNbr) = 0;
    //! Add a state to the trace
    virtual void addState  (int proc    , int state, double time) = 0;
    //! Start a link on the trace
    virtual void startLink (int proc    , int type , double time) = 0;
    //! End a link on the trace
    virtual void endLink   (int proc    , int type , double time) = 0;
    //! Add an event to the trace
    virtual void addEvent  (int proc    , int type , double time) = 0;
    //! Inc a counter to the trace
    virtual void incCpt    (int proc    , int var  , double time) = 0;
    //! Dec a counter to the trace
    virtual void decCpt    (int proc    , int var  , double time) = 0;
    //! End the container and close the file
    virtual void endTrace  ()                                     = 0;


};


#endif
