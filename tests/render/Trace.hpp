#ifndef TRACE_HPP
#define TRACE_HPP

#include "Render.hpp"
#include "Render_opengl.hpp"

class Trace{
public:
    Trace(){
        std::cout << "Trace constructor" << std::endl;
    }

    template <class B> 
    void build(B r){
        std::cout << "Trace build" << std::endl;
        r.draw();
    }

};

#endif 
