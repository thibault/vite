#include<cstdio>
#include<cstdlib>

#include "Trace.hpp"
#include "Render.hpp"
#include "Render_opengl.hpp"



int main(int argc, char** argv){

    Render<Render_opengl> r(new Render_opengl());
    Trace t;
    
    t.build(r);

    return EXIT_SUCCESS;
}
