#ifndef RENDER_HPP
#define RENDER_HPP

#include<iostream>


template<class T>
class Render{

    T* render_instance;
public:
    Render(T* instance){
        std::cout << "Render constructor" << std::endl;
        render_instance = instance;
    }

    void draw(){
        std::cout << "Render draw" << std::endl;
        render_instance->drawRect();
    }

};

#endif 
