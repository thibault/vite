/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément 
**
*/
/*!
 *\file interface/resource.hpp
 *\brief This file gives some defines for the interface classes.
 */



#ifndef INTERFACE_RESOURCE_HPP
#define INTERFACE_RESOURCE_HPP


/*!
 * \brief Checks if a function return -1 as a value.
 *
 * This macro is used with C functions to check them if an error occurs. Thus, it display the file, the line and some informations (with m variable) of the error than exit the program.
 */
#define CKF(f, m) if((f)==-1) {                                         \
        cerr << "File " << __FILE__ <<" - line : " << __LINE__ <<endl;  \
        cerr << m <<endl;                                               \
        exit(EXIT_FAILURE);                                             \
    }

/*!
 * \brief Checks if a function return NULL as a value.
 *
 * This macro is used with C functions to check them if an error occurs. Thus, it display the file, the line and some informations (with m variable) of the error than exit the program.
 */
#define CKFP(f, m) if((f)==NULL) {                                      \
        cerr << "File " << __FILE__ <<" - line : " << __LINE__ <<endl;  \
        cerr << m <<endl;                                               \
        exit(EXIT_FAILURE);                                             \
    }


#endif
