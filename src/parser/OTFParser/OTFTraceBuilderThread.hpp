/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément 
**
*/
/*!
 *\file ParserEventOTF.hpp
 *\brief This file contains the event parser used by the ParserOTF.
 */

#ifndef OTFTraceBuilderThread_HPP
#define OTFTraceBuilderThread_HPP
#include <sstream>
#include <string>
#include <map>
#include <queue>
#include <list>
/* -- */
#include <otf.h>
/* -- */
#include "common/Errors.hpp"
/* -- */
#include "trace/values/Values.hpp"
#include "trace/EntityValue.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/Trace.hpp"
/* -- */
#include "parser/OTFParser/ParserDefinitionOTF.hpp"

#include <QWaitCondition>
#include <QSemaphore>
#include <QObject>
#include <QMutex>
#include <QThread>

/*!
 * \struct Marker
 * \brief Contains the definition of a marker
 */
struct Marker {
    /*!
     * \brief Name of the marker
     */
    std::string _name;
    /*!
     * \brief stream where is defined this marker
     */
    uint32_t _stream;
    /*!
     * \brief type of the marker
     */
    uint32_t _type;
};


typedef struct OTF_Trace_builder_struct{
    int _id;
    int (*func)(OTF_Trace_builder_struct*);
    void *trace;
    uint64_t time;
    uint32_t process;
    uint32_t process2;
    uint64_t value;
    const char *text;
    uint32_t group;
    uint32_t type;
    uint32_t length;
    uint32_t source;
    OTF_Trace_builder_struct(){ }

}OTF_Trace_builder_struct;


//class to handle the real work in a second thread
class OTFTraceBuilderThread: public QObject{
Q_OBJECT
private:

    QWaitCondition* _cond;
    bool _is_finished;
    QSemaphore * _freeSlots;
    QMutex* _mutex;

    static std::map <const String, Container *, String::less_than> _containers;

    static std::map<uint32_t, Marker > _marker;
    
   public: 
  /*   static int handler_begin_process(void *trace, uint64_t time, uint32_t process);

    static int handler_end_process (void *trace, uint64_t time, uint32_t process);

    static int handler_enter (void *trace, uint64_t time, uint32_t function, uint32_t process, uint32_t process2);

    static int handler_leave (void *trace, uint64_t time, uint32_t function, uint32_t process, uint32_t process2);

    static int handler_counter(void *trace, uint64_t time, uint32_t process, uint32_t process2, uint64_t value);

    static int handler_defmarker(void *trace, uint32_t process, uint32_t token, const char *name, uint32_t type);

    static int handler_marker(void *trace, uint64_t time, uint32_t process, uint32_t token, const char *text);

    static int handler_send_message (void *trace, uint64_t time, uint32_t process, uint32_t receiver, uint32_t group, uint32_t type, uint32_t length, uint32_t source);

    static int handler_receive_message (void *trace, uint64_t time, uint32_t process, uint32_t sendProc, uint32_t group, uint32_t type, uint32_t length, uint32_t source);
    */
      static int handler_begin_process(OTF_Trace_builder_struct* tb_struct);

    static int handler_end_process (OTF_Trace_builder_struct* tb_struct);

    static int handler_enter (OTF_Trace_builder_struct* tb_struct);

    static int handler_leave (OTF_Trace_builder_struct* tb_struct);

    static int handler_counter(OTF_Trace_builder_struct* tb_struct);

    static int handler_defmarker(OTF_Trace_builder_struct* tb_struct);

    static int handler_marker(OTF_Trace_builder_struct* tb_struct);

    static int handler_send_message (OTF_Trace_builder_struct* tb_struct);

    static int handler_receive_message (OTF_Trace_builder_struct* tb_struct);



public slots:

  void build_trace(int n, OTF_Trace_builder_struct* tb_struct); 
  void build_finished(); 
   
    
public:
    /*!
     * \fn ParserEventOTF()
     * \brief constructor
     */
    OTFTraceBuilderThread(QWaitCondition* cond,  QSemaphore * freeSlots, QMutex* mutex);

    /*!
     * \fn ~ParserEventOTF()
     * \brief constructor
     */
    ~OTFTraceBuilderThread();


};

#endif
