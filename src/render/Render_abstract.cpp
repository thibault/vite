/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
/*!
 *\file Render_abstract.cpp
 */

#include <stack>
#include <sstream>
/* -- */

#include "common/common.hpp"
#include "common/Info.hpp"
/* -- */
#include "interface/Interface.hpp"
/* -- */
#include "common/Message.hpp"
/* -- */
#include "render/Render_abstract.hpp"
/* -- */
using namespace std;

#define message *Message::get_instance() << "(" << __FILE__ << " l." << __LINE__ << "): "


static const Element_pos zoom_limit = 0.000001;/* Use to control the minimum distance between
                                                   the minimum x visible and the maximum x visible. */
static const int scroll_margin = 100;/* Control the margin of the trace. Must be in percentage.
                                (i.e. between [0; 100]) */


/***********************************
 *
 *
 *
 * Constructor and destructor.
 *
 *
 *
 **********************************/

Render_abstract::Render_abstract(Core *core)
        : _core(core) {

    /* init main informations about the scene and the widget size */

    _ruler_distance = 5;
    _key_scrolling = false;
    _selection_rectangle_alpha = 0.5;
}


/***********************************
 *
 *
 *
 * Scaling and scrolling functions.
 *
 *
 *
 **********************************/


void Render_abstract::apply_zoom_box(Element_pos x_min, Element_pos x_max, Element_pos y_min, Element_pos y_max) {

    Element_pos x_middle_selection;
    Element_pos y_middle_selection;
    Element_pos x_scaled_middle_selection;
    Element_pos y_scaled_middle_selection;
    Element_pos x_distance_between_state_origin_and_render_middle;
    Element_pos y_distance_between_state_origin_and_render_middle;
    Element_pos delta;

    /*
     * Now, we try to zoom on the selection rectangle. To perform this, the left of the selection rectangle must be fit with the left
     * of the render area. Idem for the right, the top and the bottom.
     *
     * Thus, we need to know the difference the scale between the rectangle width and height and the render area width and height.
     * Results are given by the scale: Info::Screen::width/(x_max - x_min) for the horizontal dimension.
     *
     * Then, our selection rectangle is scaled. Nevertheless, it should not be centered inside the render area. So, we work out
     * the correct x and y translation to make both the render area middle and the selection rectangle middle fit.
     * There are 3 steps for the middle x position :
     *  - First, find distance between the scaled selection rectangle middle and the state origin.
     *  - Second, find the distance between the state origin and the render area middle.
     *  - Finally, translate the scaled selection middle to the difference.
     */


    /*
     * Work out the horizontal middle of the selection area (the selection area width divide by 2 plus the x left coordinate)
     * Note: mouse positions are converted to render area coordinate with screen_to_render_**() methods.
     */
    x_middle_selection = screen_to_render_x(x_min + (x_max - x_min) / 2);
    y_middle_selection = screen_to_render_y(y_min + (y_max - y_min) / 2);


    /*
     * 1st step:
     *
     * Work out the new selection middle position after applying the scale.
     */
    x_scaled_middle_selection = (x_middle_selection + _x_state_translate - _default_entity_x_translate) *
                                ((Info::Screen::width * (1 - _x_scale_container_state)) / ((x_max - x_min)));
    y_scaled_middle_selection = (y_middle_selection + _y_state_translate - _ruler_height) *
                                ((Info::Screen::height - render_to_screen_y(_ruler_height)) / ((y_max - y_min)));

    /*
     * 2nd step:
     *
     * Work out the distance between the state origin and the render area middle (Info::Render::width/2).
     */
    x_distance_between_state_origin_and_render_middle =
            _x_state_translate + Info::Render::width * _x_scale_container_state - _default_entity_x_translate +
            Info::Render::width / 2 - _default_entity_x_translate / 2;
    y_distance_between_state_origin_and_render_middle = _y_state_translate + (Info::Render::height - _ruler_height) / 2;

    /*
     * 3rd step:
     *
     * Translate entities.
     */
    _x_state_translate += x_scaled_middle_selection - x_distance_between_state_origin_and_render_middle;
    _y_state_translate += y_scaled_middle_selection - y_distance_between_state_origin_and_render_middle;


    /*
     * Finally, perform the scale.
     */

    /* NOTE: do not use replace_scale() because the translate will be also modified */
    int buf;
    _x_state_scale *= (Info::Render::width * (1 - _x_scale_container_state)) / screen_to_render_x(x_max - x_min);


    _y_state_scale *= (Info::Render::height - _ruler_height) / screen_to_render_y(y_max - y_min);


    /* Now, check if zoom is not too much */
    update_visible_interval_value();

    delta = Info::Render::_x_max_visible - Info::Render::_x_min_visible;

    if (delta <= zoom_limit) {/* if too much, cancel */

        _x_state_scale *= (x_max - x_min) / Info::Screen::width;
        _y_state_scale *= (y_max - y_min) / Info::Screen::height;
        _x_state_translate -= (x_scaled_middle_selection - x_distance_between_state_origin_and_render_middle);
        _y_state_translate -= (y_scaled_middle_selection - y_distance_between_state_origin_and_render_middle);
    }
#if defined(USE_ITC) && defined(BOOST_SERIALIZE)
    if(Info::Splitter::preview==true && Info::Render::_key_ctrl == true){
        //we are in preview mode, we have to load the actual data from the serialized files
        Info::Splitter::_x_min= Info::Render::_x_min_visible;
        Info::Splitter::_x_max= Info::Render::_x_max_visible;
      //  Info::Splitter::preview=false;
        _core->launch_action(_core->_LOAD_DATA, NULL);
        Info::Render::_key_ctrl =false;
       // return;
        }
#endif

    buf = (int) (100 * _x_state_scale);
    _core->launch_action(_core->_STATE_ZOOM_BOX_VALUE, &buf);
    /* Update render */
    update_render();
    refresh_scroll_bars(true);
}

void Render_abstract::apply_zoom_on_interval(Element_pos t1, Element_pos t2) {

    Element_pos x_min = render_to_screen_x(trace_to_render_x(t1));
    Element_pos x_max = render_to_screen_x(trace_to_render_x(t2));
    Element_pos y_min = render_to_screen_y(_ruler_height + _ruler_y);
    Element_pos y_max = Info::Screen::height;

    apply_zoom_box(x_min, x_max, y_min, y_max);
}


void Render_abstract::change_scale(Element_pos scale_coeff) {
    Element_pos new_scale;

    new_scale = _x_state_scale * (1 + scale_coeff * 0.05);/* 5% scale */

    if (new_scale < 1.0)
        replace_scale(1.0);
    else
        replace_scale(new_scale);
}

void Render_abstract::change_scale_y(Element_pos scale_coeff) {

    Element_pos new_scale;

    new_scale = _y_state_scale * (1 + scale_coeff * 0.05);/* 5% scale */

    if (new_scale < 1.0)
        _y_state_scale = 1.0;
    else
        replace_scale_y(new_scale);
}


void Render_abstract::replace_scale(Element_pos new_scale) {
    int buf;

    if (new_scale > 0.0) {
        /*
         * Reajust the entity translation to recenter the same point previously under the mouse pointer
         *
         * Work out the distance between the point in the middle Widget and the state origin.
         * Then multiply it by the scale coefficient ( (new_scale / old_scale) ) less 1.
         * 1 corresponding to the original translation of the point from the state origin.
         * Finally, translate the result.
         *
         */
        _x_state_translate += (_x_state_translate - _default_entity_x_translate +
                               (Element_pos) Info::Render::width / 2.0) * ((new_scale) / (_x_state_scale) - 1);
        _x_state_scale = new_scale;

        buf = (int) (100 * _x_state_scale);

        _core->launch_action(_core->_STATE_ZOOM_BOX_VALUE, &buf);


        refresh_scroll_bars(true);
    }
}

void Render_abstract::replace_scale_y(Element_pos new_scale) {

    if (new_scale > 0.0) {
        /*
         * Reajust the entity translation to recenter the same point previously under the mouse pointer
         *
         * Work out the distance between the point in the middle Widget and the state origin.
         * Then multiply it by the scale coefficient ( (new_scale / old_scale) ) less 1.
         * 1 corresponding to the original translation of the point from the state origin.
         * Finally, translate the result.
         *
         */
        _y_state_translate += (_y_state_translate - _ruler_height + (Element_pos) Info::Render::height / 2.0) *
                              ((new_scale) / (_y_state_scale) - 1);
        _y_state_scale = new_scale;

        /* TODO add a zoom box value for y zoom */
        update_render();
        refresh_scroll_bars(true);
    }
}


void Render_abstract::change_translate(int translate) {
    replace_translate(_x_state_translate + translate);
}


void Render_abstract::change_translate_y(int translate) {
    replace_translate_y(_y_state_translate + translate);
}


void Render_abstract::replace_translate(Element_pos new_translate) {
    if (new_translate < (-scroll_margin * _x_state_scale)) {
        new_translate = -scroll_margin * _x_state_scale;
    } else if (new_translate > (scroll_margin * _x_state_scale)) {
        new_translate = scroll_margin * _x_state_scale;
    }

    _x_state_translate = new_translate;

    if (_key_scrolling) {
        refresh_scroll_bars();
        _key_scrolling = false;
    }
    update_render();
}


void Render_abstract::replace_translate_y(Element_pos new_translate) {
    if (new_translate < (-scroll_margin * _y_state_scale)) {
        new_translate = -scroll_margin * _y_state_scale;
    } else if (new_translate > (scroll_margin * _y_state_scale)) {
        new_translate = scroll_margin * _y_state_scale;
    }

    _y_state_translate = new_translate;

    if (_key_scrolling) {
        refresh_scroll_bars();
        _key_scrolling = false;
    }
    update_render();
}


void Render_abstract::registered_translate(int id) {

    switch (id) {

        case Info::Render::X_TRACE_BEGINNING:/* show the beginning entities */
            _x_state_translate = _default_entity_x_translate - (Element_pos) Info::Render::width / 2.0;
            break;
        case Info::Render::Y_TRACE_BEGINNING:/* show the beginning entities */
            _y_state_translate = (Element_pos) Info::Render::height / 2.0 - _ruler_height;
            break;
        case Info::Render::X_TRACE_ENDING:/* show the ending entities */
            //        _x_state_translate =  (_default_entity_x_translate - (Element_pos)Info::Render::width/2.0 + _state_x_max*_x_state_scale
            //                   *((Info::Render::width-_default_entity_x_translate)/(_state_x_max-_state_x_min)));
            _x_state_translate = (_default_entity_x_translate - (Element_pos) Info::Render::width / 2.0 +
                                  Info::Entity::x_max * coeff_trace_render_x());//_x_state_scale
            //                             *((Info::Render::width-_default_entity_x_translate)/(_state_x_max-_state_x_min)));

            break;
        case Info::Render::Y_TRACE_ENDING:/* show the ending entities */
            _y_state_translate = (-_ruler_height + (Element_pos) Info::Render::height / 2.0 -
                                  _state_y_max * _y_state_scale
                                  * ((Info::Render::height - _ruler_height) / (_state_y_max - _state_y_min)));
            break;
        case Info::Render::X_TRACE_ENTIRE:/* show the entire entities */
            //        replace_translate(0);//_x_state_translate = 0;
            replace_scale(1);//_x_state_scale = 1;
            replace_translate(0);
            break;
        case Info::Render::Y_TRACE_ENTIRE:/* show the entire entities */
            /*        _y_state_translate = 0;
                      _y_state_scale = 1; */
            replace_scale_y(1);
            replace_translate_y(0);
            break;
        default:
            message << "Unknown registered translate" << Message::endw;
    }

    refresh_scroll_bars();
    update_render();
}


void Render_abstract::refresh_scroll_bars(bool length_changed) {
    if (length_changed) {
        Element_pos scroll_bar_length[2] = {_x_state_scale * scroll_margin, _y_state_scale * scroll_margin};
        _core->launch_action(_core->_STATE_AJUST_SCROLL_BARS, scroll_bar_length);
    }

    _x_scroll_pos = _x_state_translate;
    _y_scroll_pos = _y_state_translate;
    Element_pos buf[2] = {_x_scroll_pos, _y_scroll_pos};
    _core->launch_action(_core->_STATE_REFRESH_SCROLL_BARS, buf);
}


void Render_abstract::change_scale_container_state(int view_size) {

    _x_scale_container_state = 0.01 * view_size;
    update_render();
}
