/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/


#ifndef RENDER_ABSTRACT_HPP
#define RENDER_ABSTRACT_HPP
#include "render/Geometry.hpp"
#include "render/Render_windowed.hpp"
#include "core/Core.hpp"


/*!
 * \brief This class redefined the OpenGL widget - QGLWidget - to display the trace.
 */
class Render_abstract : public Geometry, public Render_windowed
{
    //Q_OBJECT

protected:


    /*!
     * \brief Contains the parent instance.
     */
    Core* _core;


    /***********************************
     *
     * Render area state attributes.
     *
     **********************************/

    /*!
     * \brief Indicate if the scroll was asked by key capture in openGL render area.
     */
    bool _key_scrolling;

    /*!
     * \brief Alpha color of the selection rectangle.
     */
    double _selection_rectangle_alpha;

    /*!
     * \brief Scale the current view to the zoom box shape.
     * \param x_min the x minimum position.
     * \param x_max the x maximum position.
     * \param y_min the y minimum position.
     * \param y_max the y maximum position.
     */
    virtual void apply_zoom_box(Element_pos x_min, Element_pos x_max, Element_pos y_min, Element_pos y_max);

public:

    /***********************************
     *
     * Constructor and destructor.
     *
     **********************************/

    /*!
     * \brief The constructor.
     * \param render_instance The instance of a drawing class.
     */
    Render_abstract(Core* core);



    /***********************************
     *
     * Scaling and scrolling functions.
     *
     **********************************/


    /*!
     * \brief Change the scale of state drawing.
     * \param scale_coeff The new coefficient scale value to add to the current scale.
     */
    void change_scale(Element_pos scale_coeff) override;

    /*!
     * \brief Change the scale of the y state drawing.
     * \param scale_coeff The new coefficient scale value to add to the current scale.
     */
    void change_scale_y(Element_pos scale_coeff) override;

    /*!
     * \brief Replace the current scale by a new scale. (horizontally)
     * \param new_scale The new scale value to replace the current scale.
     */
    void replace_scale(Element_pos new_scale) override;

    /*!
     * \brief Replace the current scale by a new scale. (vertically)
     * \param new_scale The new scale value to replace the current scale.
     */
    void replace_scale_y(Element_pos new_scale) override;

    /*!
     * \brief Change the x position of camera view for state drawing area.
     * \param translate The new position.
     */
    void change_translate(int translate) override;/* temporary -> to change the translate to view states */

    /*!
     * \brief Change the y position of camera view for state drawing area.
     * \param translate The new position.
     */
    void change_translate_y(int translate);/* temporary -> to change the translate to view states */

    /*!
     * \brief Replace the current x translate by a new translate.
     * \param new_translate The new translate value to replace the current translate.
     */
    void replace_translate(Element_pos new_translate) override;

    /*!
     * \brief Replace the current y translate by a new translate.
     * \param new_translate The new translate value to replace the current translate.
     */
    void replace_translate_y(Element_pos new_translate) override;

    /*!
     * \brief Pre registered translation values (for x or y translate).
     * \param id The pre registered translation id.
     */
    void registered_translate(int id) override;

    /*!
     * \brief Refresh scroll bar positions when shortcuts execute movements
     * \param length_changed If true the total length of the scroll bar will be updated.
     */
    void refresh_scroll_bars(bool length_changed = false) override;

    /*!
     * \brief Change the percentage taken by container display in the render area.
     * \param view_size The new percentage (between 0 to 100).
     */
    void change_scale_container_state(int view_size) override;/* temporary -> to change the size of container view */

    void apply_zoom_on_interval(Element_pos t1, Element_pos t2) override;

};



#endif
