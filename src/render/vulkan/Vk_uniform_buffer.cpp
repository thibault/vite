/*
 ** This file is part of the ViTE project.
 **
 ** This software is governed by the CeCILL-A license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 **
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 **
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-A license and that you accept its terms.
 **
 **
 ** ViTE developers are (for version 0.* to 1.0):
 **
 **        - COULOMB Kevin
 **        - FAVERGE Mathieu
 **        - JAZEIX Johnny
 **        - LAGRASSE Olivier
 **        - MARCOUEILLE Jule
 **        - NOISETTE Pascal
 **        - REDONDY Arthur
 **        - VUCHENER Clément
 **
 */
#include "render/vulkan/Vk_uniform_buffer.hpp"

Vk_uniform_buffer::Vk_uniform_buffer::Vk_uniform_buffer()
    : Vk_buffer(VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT) { }

void Vk_uniform_buffer::init_uniform(const VkDescriptorSetAllocateInfo *descriptor_info, uint32_t concurrent_frame_count, VkDeviceSize alloc_size)
{
    alloc_buffer(concurrent_frame_count * alloc_size);

    VkDeviceSize mem_size_uniform = get_mem_size();
    quint8 *buffer_data;
    VkResult err = _dev_funcs->vkMapMemory(_dev, _buf_mem, 0, mem_size_uniform, 0, reinterpret_cast<void **>(&buffer_data));
    if (err != VK_SUCCESS)
        qFatal("Failed to map memory: %d", err);

    // Initialize uniform data
    QMatrix4x4 ident;
    memset(_uniform_buf_info, 0, sizeof(_uniform_buf_info));
    for (uint32_t frame_index = 0; frame_index < concurrent_frame_count; ++frame_index) {
        const VkDeviceSize offset = frame_index * alloc_size;
        memcpy(buffer_data + offset, ident.constData(), 16 * sizeof(float));
        memcpy(buffer_data + offset + 16 * sizeof(float), ident.constData(), 16 * sizeof(float));
        _uniform_buf_info[frame_index].buffer = _buffer;
        _uniform_buf_info[frame_index].offset = offset;
        _uniform_buf_info[frame_index].range = alloc_size;
    }
    _dev_funcs->vkUnmapMemory(_dev, _buf_mem);

    // Allocate resources for each frame
    for (uint32_t frame_index = 0; frame_index < concurrent_frame_count; ++frame_index) {
        
        err = _dev_funcs->vkAllocateDescriptorSets(_dev, descriptor_info, &_desc_set[frame_index]);
        if (err != VK_SUCCESS)
            qFatal("Failed to allocate descriptor set: %d", err);

        VkWriteDescriptorSet desc_write = {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = _desc_set[frame_index],
            .descriptorCount = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .pBufferInfo = &_uniform_buf_info[frame_index]
        };
        _dev_funcs->vkUpdateDescriptorSets(_dev, 1, &desc_write, 0, nullptr);
    }
}

void Vk_uniform_buffer::set_variable(uint32_t frame_index, VkDeviceSize offset, VkDeviceSize size, const void* value) {
    quint8 *buffer_data;
    VkResult err = _dev_funcs->vkMapMemory(_dev, _buf_mem, _uniform_buf_info[frame_index].offset + offset, size, 0, reinterpret_cast<void **>(&buffer_data));
    if (err != VK_SUCCESS)
        qFatal("Failed to map memory: %d", err);

    memcpy(buffer_data, value, size);
    _dev_funcs->vkUnmapMemory(_dev, _buf_mem);
}

void Vk_uniform_buffer::bind_uniform_buffer(VkCommandBuffer &command_buffer, uint32_t frame_index, VkPipelineLayout &pipeline_layout) {
    _dev_funcs->vkCmdBindDescriptorSets(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline_layout, 0, 1, &_desc_set[frame_index], 0, nullptr);
}
