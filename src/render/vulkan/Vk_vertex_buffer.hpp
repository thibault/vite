/*
 ** This file is part of the ViTE project.
 **
 ** This software is governed by the CeCILL-A license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 **
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 **
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-A license and that you accept its terms.
 **
 **
 ** ViTE developers are (for version 0.* to 1.0):
 **
 **        - COULOMB Kevin
 **        - FAVERGE Mathieu
 **        - JAZEIX Johnny
 **        - LAGRASSE Olivier
 **        - MARCOUEILLE Jule
 **        - NOISETTE Pascal
 **        - REDONDY Arthur
 **        - VUCHENER Clément
 **
 */
#ifndef VK_VERTEX_BUFFER_HPP
#define VK_VERTEX_BUFFER_HPP

#include <vector>
#include "render/vulkan/Vk_buffer.hpp"

class Vk_vertex_buffer: public Vk_buffer {

private:
	uint32_t _vertex_count = 0;
public:
	Vk_vertex_buffer();

	uint32_t vertex_count();
	
	void bind_vertex_buffer(VkCommandBuffer* command_buffer);

	void draw(VkCommandBuffer* command_buffer);

	template<class V>
	void set_data(std::vector<V> data) {
		int size = data.size() * sizeof(V);
		if (_vertex_count != data.size())
		{
			_vertex_count = data.size();
			realloc_buffer(size);
		}

		if (_vertex_count == 0)
			return;
		
		quint8 *buffer_data;
		VkResult err = _dev_funcs->vkMapMemory(_dev, _buf_mem, 0, size, 0, reinterpret_cast<void **>(&buffer_data));
		if (err != VK_SUCCESS)
			qFatal("Failed to map memory: %d", err);

		memcpy(buffer_data, data.data(), size);
		_dev_funcs->vkUnmapMemory(_dev, _buf_mem);
	}
};

#endif