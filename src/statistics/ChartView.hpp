#pragma once

#include <QtWidgets/qscrollbar.h>
#include <QtCharts/qchartview.h>
#include <QtWidgets/qapplication.h>
#include <QObject>

class Chart_View : public QtCharts::QChartView {
    Q_OBJECT
private:
    qreal _factor=1.0;
    QPoint _last_mouse_pos;

    bool _restrict_scroll_x = true;
    bool _restrict_scroll_y = false;
    bool _ignore_scrollbar = false;

    QScrollBar& _x_scroll;
    QScrollBar& _y_scroll;

    QPointF _scroll_value;

public:
    Chart_View(
        QScrollBar& x_scroll,
        QScrollBar& y_scroll,
        QtCharts::QChart *chart,
        QWidget *parent = nullptr
    );

    void restrict_scroll(bool x, bool y);

    void reset_scroll_zoom();

signals:
    void chart_focused();

protected:
    virtual void focusInEvent(QFocusEvent* event) override;

    void wheelEvent(QWheelEvent* event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
public slots:
    void on_x_scroll_changed(int x);
    void on_y_scroll_changed(int x);
};