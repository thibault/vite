/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
/*!
 * \file Command_window.hpp
 */

#ifndef _COMMAND_WINDOW_HPP
#define _COMMAND_WINDOW_HPP

class Command_window;

#include "ui_global_cmd.h"

/*!
 * \brief Class that is used to execute an exterior command on the trace
 */

class Interface_graphic;
class Container;
class Trace;
class QTreeWidgetItem;

class Command_window : public QMainWindow, protected Ui::Execution{

    Q_OBJECT

private :
    Trace* _trace;
    QStringList _cmd;
    QLineEdit* _ui_cmd_box;
    QLineEdit* _ui_start_box;
    QLineEdit* _ui_end_box;
    QLineEdit* _ui_filter_box;
    QTreeWidget* _ui_tree_box;
    std::vector<const Container *> _selected_containers;
    Interface_graphic *_console;

    void set_container_names();
    void set_container_names_rec(QTreeWidgetItem *current_node, const Container *current_container);
    void show_error();
public :
    void set_selected_nodes();
    Command_window( QWidget *parent, Interface_graphic * i);

    void init_window();
    void set_trace(Trace * t);

private slots :
    void on_execute_button_clicked();
};

#endif
