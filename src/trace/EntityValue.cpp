/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
#include <string>
#include "stdio.h"
#include <list>
#include <iostream>
#include <map>
/* -- */
#include "trace/values/Values.hpp"
#include "trace/ContainerType.hpp"
#include "trace/EntityType.hpp"
#include "trace/EntityValue.hpp"
#include "common/Session.hpp"
#include "common/Palette.hpp"
/* -- */
using namespace std;

EntityValue::EntityValue(const Name &name, EntityType *type, map<std::string, Value *> opt)
    : _name(name), _type(type), _opt(opt), _visible(true)
{
    Palette    *sp    = nullptr;
    Color      *color = nullptr;
    std::string palette_name = "default";

    // Search opt field for color to set the default (random if not provided within the trace file)
    map<std::string, Value *>::iterator it = opt.find(string("Color"));
    if ( it != opt.end() ) {
        _filecolor = (Color*)(it->second);
        opt.erase(it);
    } else {
        if (type->get_class() == _EntityClass_State)
            _filecolor = new Color();
        else
            _filecolor = new Color(1., 1., 1.);
    }

    // Set the used color to the color selected in the palette if existing, otherwise equal to the filecolor.
    _usedcolor = _filecolor;

    switch (type->get_class()) {
    case _EntityClass_State:
        palette_name = "palette";
        break;
    case _EntityClass_Link:
        palette_name = "link_types";
        break;
    case _EntityClass_Event:
        palette_name = "event_types";
        break;
    case _EntityClass_Variable:
        break;
    default:
        std::cerr << "Error unsupported type for Entity value" << std::endl;
        break;
    }

    if ( type->get_class() != _EntityClass_Variable ) {
        sp = Session::get_palette( palette_name,
                                   Session::get_current_palette( palette_name ) );
        color = sp->get_color( name.get_name() );
        if( color != nullptr ) {
            _usedcolor = new Color( color );
        }
        if( sp->is_visible( name.get_name() ) ) {
            _visible = true;
        }
        else {
            _visible = false;
        }
    }
}

EntityValue::~EntityValue()
{
    _type = nullptr;

    for(auto & it : _opt){
        delete it.second;
    }
}

Name
EntityValue::get_Name() const
{
    return _name;
}

std::string
EntityValue::get_name() const
{
    return _name.get_name();
}

std::string
EntityValue::get_alias() const
{
    return _name.get_alias();
}

const EntityType *
EntityValue::get_type() const
{
    return _type;
}

const map<string, Value *> *
EntityValue::get_extra_fields() const
{
    return &_opt;
}

Color*
EntityValue::get_used_color() const
{
    return _usedcolor;
}

Color*
EntityValue::get_file_color() const
{
    return _filecolor;
}

bool
EntityValue::get_visible() const
{
    return _visible;
}

EntityClass_t
EntityValue::get_class() const
{
    return _type->get_class();
}

void
EntityValue::set_used_color(Color *c)
{
    if( (_usedcolor != _filecolor) &&
        (_usedcolor != nullptr) ) {
        delete _usedcolor;
    }
    _usedcolor = c;

    emit changedColor( this );
}

void
EntityValue::set_visible(bool b)
{
    if ( _visible != b ) {
        _visible = b;
        emit changedVisible( this );
    }
}

void
EntityValue::reload_file_color()
{
    if( (_usedcolor != _filecolor) &&
        (_usedcolor != nullptr) ) {
        delete _usedcolor;
    }
    _usedcolor = _filecolor;
}

