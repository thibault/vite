/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Cl�ment
**
*/


/*
 * \file SerializerDispatcher.hpp
 * Class that handles multiple threads for parallel serialization
 * A container calls its methods, which are executed on a free thread or polled if we need to wait
 */

#include <QObject>

#include "trace/Serializer.hpp"

struct IntervalOfContainer;
class SerializerWriter;
class SerializerDispatcher:  public QObject,public Singleton <SerializerDispatcher>  {
Q_OBJECT

// the array of objects to send the data to (all of them have their own thread)
 SerializerWriter* _evt_array;
// the index of the last one called, in order to avoid calling it just after
 unsigned int _previous_call;
 //the number of threads, usually the number of CPUs
 unsigned int _nb_threads;
 //check the state
 bool _killed;

 QMutex* _mutex;
//static unsigned int nb_cpus=boost::thread::hardware_concurrency();

public:
    SerializerDispatcher();
    ~SerializerDispatcher();
    void init();
     void launch_threads();
     void kill_all_threads();
     //dump an itc, after choosing a free thread to use
     void dump(IntervalOfContainer* itc, char* );
     void load(IntervalOfContainer*, char*);
     signals:
     void load_data(IntervalOfContainer*, char*);
     void dump_on_disk(IntervalOfContainer*, char*);
     void build_finish();
};
