/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
#ifndef STRING_HPP
#define STRING_HPP

/*!
 *
 * \file String.hpp
 *
 */
/*!
 *
 * \class String
 * \brief Store a string in the trace
 *
 */
class String: public Value {
private:
    std::string _value;

public:

    /*!
     * \struct less_than
     * \brief Comparator for map
     */
    struct less_than {
        /*!
         * \fn operator()(const String &, const String &) const
         * \brief Returns true if the second string is greater than the first one
         */
        bool operator()(const String &, const String &) const;
    };

    /*!
     * \brief Constructor
     */
    String();

    /*!
     * \brief Constructor
     */
    String(std::string);

    /*!
     * \fn to_string() const
     */
    std::string to_string() const override;


    /*!
     * \fn operator ==(const std::string &) const
     * \return true if they are equals.
     */
    bool operator ==(const std::string &) const;

    /*!
     * \fn operator= (const std::string &)
     * \return this String with the new value.
     */
    String operator= (const std::string &);

};

#endif // STRING_HPP
