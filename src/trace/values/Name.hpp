 /*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
#ifndef NAME_HPP
#define NAME_HPP
/*!
 *
 * \file Name.hpp
 *
 */
/*!
 *
 * \class Name
 * \brief Store a name and/or an alias for trace objects
 *
 */
 class String;

class Name: public Value {
private:
    std::string _alias, _name;

public:
    /*!
     * \brief Constructor
     */
    Name();
    Name(std::string name, std::string alias);
    Name(const std::string &name);
    Name(const String &name);

    /*!
     * \fn set_name(std::string name)
     * \brief Set the name
     * \param name Name of the object
     *
     */
    void set_name(std::string name);

    /*!
     * \fn set_alias(std::string alias)
     * \brief Set the alias
     * \param alias Alias of the object
     *
     */
    void set_alias(std::string alias);

    /*!
     *
     * \fn to_string() const
     * \return The name of the object, if it exists, the alias otherwise
     *
     */
    std::string to_string() const override;

    /*!
     *
     * \fn to_string() const
     * \return The name of the object
     *
     */
    std::string get_name() const;

        /*!
     *
     * \fn to_string() const
     * \return The alias
     *
     */
    std::string get_alias() const;

    /*!
     *
     * \fn to_String() const
     * \return The name of the object, if it exists, the alias otherwise
     *
     */
    String to_String() const;

    /*!
     *
     * \fn operator== (String &) const
     * \brief Compare the string with the name or the alias
     * \return true if the string equals the name or the alias
     *
     */
    bool operator== (String &) const;
    bool operator== (Name &) const;

    /*!
     *
     * \fn operator== (std::string &) const
     * \brief Compare the string with the name or the alias
     * Provided for convenience
     * \return true if the string equals the name or the alias
     *
     */
    bool operator== (std::string &) const;

    bool operator< (std::string &s) const ;
    bool operator< (const Name &s) const ;

    friend std::ostream& operator<<(std::ostream& os, const Name& obj);
};

#endif // NAME_HPP
