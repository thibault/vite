/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
#ifndef DOUBLE_HPP
#define DOUBLE_HPP

/*!
 *
 * \file Double.hpp
 *
 */
/*!
 *
 * \class Double
 * \brief Store a double in the trace
 *
 */
class Double: public Value {
private :
    double _value {0.0};

public:
    /*!
     * \brief Constructor
     */
    Double();

    /*!
     * \brief Constructor
     */
    Double(double);

    /*!
     * \brief Constructor
     */
    Double(const std::string &value);

    /*!
     *
     * \fn to_string() const
     * \return a string of the double
     */
    std::string to_string() const override;

    /*!
     *
     * \fn get_value() const
     * \return the value
     *
     */
    double get_value() const;

    /*!
     *
     * \fn operator+ (const Double &) const
     * \brief Computes the sum between two Doubles
     * \return a double which is equal to the sum of the two Doubles.
     *
     */
    Double operator+(const Double &) const;

    /*!
     *
     * \fn operator- () const
     * \return a Double which is equal to the opposite of this Double.
     *
     */
    Double operator-() const;

    /*!
     *
     * \fn operator- (const Double &) const
     * \brief Make the difference between two Doubles
     * \return a Double which is equal to the difference of the two values.
     *
     */
    Double operator-(const Double &) const;

    /*!
     *
     * \fn operator< (const Double &) const
     * \brief Compare the date
     * \return true if the Double is lower than this.
     *
     */
    bool operator<(const Double &) const;

    /*!
     *
     * \fn operator> (const Double &) const
     * \brief Compare the date
     * \return true if the Double is greater than this.
     *
     */
    bool operator>(const Double &) const;
};

#endif // DOUBLE_HPP
