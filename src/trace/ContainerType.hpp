/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
#ifndef CONTAINERTYPE_HPP
#define CONTAINERTYPE_HPP

/*!
 * \file ContainerType.hpp
 */
/*!
 * \class ContainerType
 * \brief Describe the type of a container
 */
class ContainerType {
private:
    Name                       _name;
    ContainerType             *_parent;
    std::list<ContainerType *> _children;

public:
    /*!
     * \fn ContainerType(Name &name, ContainerType *parent)
     * \brief Constructor of ContainerType
     * \param name Name of the container type
     * \param parent Type of the parent container
     */
    ContainerType(Name &name, ContainerType *parent);

    /*!
     * \fn ~ContainerType()
     * \brief Destructor
     */
    ~ContainerType();

    /*!
     * \fn add_child(ContainerType *child)
     * \brief Add a type of child container
     * \param child Type of child container
     */
    void add_child(ContainerType *child);

    /*!
     * \fn get_name()
     * \brief name accessor
     */
    const Name get_Name() const;
    const Name get_name() const;

    /*!
     * \fn get_parent() const
     * \brief Get the parent container type
     */
    const ContainerType *get_parent() const;

    /*!
     * \fn get_children() const
     * \brief Get the list of the child container types
     */
    const std::list<ContainerType *> *get_children() const;
};

#endif
