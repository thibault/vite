/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
#ifndef VARIABLE_HPP
#define VARIABLE_HPP

/*!
 * \file Variable.hpp
 */

class Variable;


/*!
 * \class Variable
 * \brief Describe a variable
 */
class Variable: public Entity {
private:
    std::list<std::pair<Date, Double> > _values;
    Double _min, _max;
    VariableType *_type;

public:
    /*!
     * \brief Constructor
     * \param container Container of this variable
     * \param type Type of this variable
     */
    Variable(Container *container, VariableType *type);

    /*!
     * \brief Destructor
     */
    ~Variable();

    /*!
     * \brief Add a new value to the variable
     * \param time Date of the new value
     * \param value The new value
     */
    void add_value(Date time, Double value);

    /*!
     * \brief Get the latest value of the variable
     */
    Double get_last_value() const;

    /*!
     * \brief Get the values of the variable
     */
    const std::list<std::pair<Date, Double> > *get_values() const;
    /*!
     * \brief Get the value at a given time
     */
    double get_value_at(double d) const ;

    /*!
     * \brief Get the minimum of the values
     */
    Double get_min() const;

    /*!
     * \brief Get the maximum of the values
     */
    Double get_max() const;

    /*!
     * \brief Get the type of the variable
     */
    VariableType *get_type() const;

};

#endif
