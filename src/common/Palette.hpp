#ifndef PALETTE_HPP
#define PALETTE_HPP

class Color;

/*!
 * \class Palette
 * \brief Contains pairs of states/colors.
 */
class Palette {
    friend class Session;
private:

    std::string _name;
    std::map<std::string, Color *> _pairs;
    std::map<std::string, bool> _visible;

    //bool _has_changed; ->for save?
    Palette(const std::string &name);

public:

    ~Palette();

    //static std::string *getAvailableList() const;

    Color *get_color(const std::string &state_name);
    void add_state(const std::string &state, Color &c, bool visible);
    void clear();
    bool is_visible(const std::string &state_name);
    std::string get_name() const ;
    std::map<std::string, Color *> get_map() const;
};

#endif // PALETTE_HPP
